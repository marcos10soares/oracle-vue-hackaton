import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'



Vue.use(Router)
Vue.use(Buefy)

export default new Router({
    routes: [{
            path: '/stats',
            name: 'stats',
            component: Home
        },
        {
            path: '/',
            name: 'list',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import ( /* webpackChunkName: "about" */ './views/List.vue')
        }
    ]
})